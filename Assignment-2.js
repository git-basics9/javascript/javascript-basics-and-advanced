class Person {
    constructor(name, age, salary, sex) {
        this.name = name
        this.age = age
        this.salary = salary
        this.sex = sex
    }
    static partition(arr, field, order, pivot) {
        const leftArr = []
        const rightArr = []
        for (let i = 0; i < arr.length - 1; i++) {
            let ele = arr[i]
            if (field == 'name') {
                if (order === 'asc' ? ele.name.localeCompare(pivot.name) <= 0 : ele.name.localeCompare(pivot.name) >= 0) leftArr.push(ele)
                else rightArr.push(ele)
            } else {
                if (order === 'asc' ? ele[field] <= pivot[field] : ele >= pivot) leftArr.push(ele)
                else rightArr.push(ele)
            }
        }
        return { leftArr: leftArr, rightArr: rightArr }
    }

    static quickSort(arr, field, order) {
        //Edge Case
        if (arr.length === 1) {
            return arr
        }
        let pivot = arr[arr.length - 1]
        const { leftArr, rightArr } = this.partition(arr, field, order, pivot)
        console.log("Array Returned", leftArr, rightArr, pivot);
        if (leftArr.length > 0 & rightArr.length > 0) {
            return [...this.quickSort(leftArr, field, order), pivot, ...this.quickSort(rightArr, field, order)]
        } else if (leftArr.length > 0) {
            return [...this.quickSort(leftArr, field, order), pivot]
        } else {
            return [pivot, ...this.quickSort(rightArr, field, order)]
        }
    }
}

const Persons = [
    new Person("Ishan", 21, 140, "Male"),
    new Person("Akanksha", 13, 170, "Female"),
    new Person("Rahul", 22, 120, "Male")
]
console.log(Person.quickSort(Persons, 'name', 'asc'));
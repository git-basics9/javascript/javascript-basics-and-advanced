class Person {
    constructor(name, age, salary, sex) {
        this.name = name
        this.age = age
        this.salary = salary
        this.sex = sex
    }


    static partition(arr, low, high, field, order) {
        let pivot = arr[low]
        let i = low
        let j = high
        console.log(arr, i, j);
        while (i < j) {
            if (field == 'name') {
                while (order == 'asc' ? arr[i].name.localeCompare(pivot.name) <= 0 & i < j : arr[i].name.localeCompare(pivot.name) >= 0 & i < j) {
                    i += 1
                }
                while (order == 'asc' ? arr[j].name.localeCompare(pivot.name) > 0 : arr[j].name.localeCompare(pivot.name) < 0) {
                    j -= 1
                }
            } else {
                while (order === 'desc' ? arr[i][field] >= pivot[field] : arr[i][field] <= pivot[field]) {
                    i += 1
                    console.log("i is", i);
                }
                while (order == "desc" ? arr[j][field] < pivot[field] : arr[j][field] > pivot[field]) {
                    j -= 1
                    console.log("j is", j);
                }
            }
            if (i < j) {
                [arr[i], arr[j]] = [arr[j], arr[i]]
                console.log("Swapping i and j", i, j, arr);
            }
        }
        [arr[low], arr[j]] = [arr[j], arr[low]]
        console.log("Swapped Pivot", i, j, low, arr);
        return j
    }

    static quickSort(arr, low, high, field, order) {
        if (low < high) {
            let pivotIndex = this.partition(arr, low, high, field, order)
            this.quickSort(arr, low, pivotIndex - 1, field, order)
            this.quickSort(arr, pivotIndex + 1, high, field, order)
        }
        return arr
    }

}
const Persons2 = [
    new Person("Ishan", 3, 140, "Male"),
    new Person("Sanya", 2, 123, "Female"),
    new Person("Rahul", 4, 122, "Male"),
    new Person("Saurav", 5, 121, "Male"),
    new Person("Aditya", 10, 134, "Male")
]
console.log(Person.quickSort(Persons2, low = 0, high = Persons2.length - 1, field = 'name', order = "asc"))
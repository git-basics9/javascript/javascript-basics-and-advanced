const passwordInput = document.getElementById('InputPassword')
console.log(passwordInput);
var passwd = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,30}$/;

// passwordInput.addEventListener('input', (e) => {
//     console.log(e.target.value);
// })


const checkList = document.querySelectorAll('.check-box');
const userRadioList = document.querySelectorAll('.user_type');
const genderRadioList = document.querySelectorAll('.gender_btn');
const submitButton = document.getElementById('submit_btn')
const password_validation = () => {
    console.log("password");
    if (passwordInput.value.match(passwd)) {
        return true
    } else {
        document.getElementById('error_msg').innerHTML += '<li>Password Should be of min 6 characters with one Upper case, lower Case and special Character</li>'
        return false
    }
}

const checkbox_validation = () => {
    console.log("entered");
    var count = 0;
    for (element of checkList) {
        console.log(element);
        if (element.checked == true) {
            count += 1
        }
    }
    if (count < 2) {
        console.log(count);
        document.getElementById('error_msg').innerHTML += '<li>Select Atleast 2 Permissions</li>'
        return false
    }
    return true
}

const validate_radio_btn = (arr, err_message) => {
    var count = 0;
    for (element of arr) {
        if (element.checked == true) {
            count += 1
        }
    }
    if (count == 0) {
        document.getElementById('error_msg').innerHTML += `<li>${err_message}</li>`
        return false
    }
    return true
}


submitButton.addEventListener('click', (e) => {
    document.getElementById('error_msg').innerHTML = ' '
    if (password_validation() & checkbox_validation() & validate_radio_btn(userRadioList, 'Select User Type') & validate_radio_btn(genderRadioList, "Select Gender")) {
        document.getElementById('error_msg').innerHTML = ' '
    } else {
        e.preventDefault()
    }

})